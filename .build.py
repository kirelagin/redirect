#!/usr/bin/env python3

import os, os.path
from string import Template


OUT = 'public'
TEMPLATE = Template('''<!doctype html>
<html>
  <head>
    <meta charset=utf-8>
    <title>Redirect to $url</title>
    <meta http-equiv='refresh' content='0; url=$url'>
  </head>
</html>''')

def gen_redirect(name, url):
  d = os.path.join(OUT, name)
  os.makedirs(d)
  with open(os.path.join(d, 'index.html'), 'wt') as f:
    print(TEMPLATE.substitute({'url': url}), file=f)

if __name__ == '__main__':
  with open('redirects.txt', 'rt') as f:
    for line in f:
      if not line.strip() or line.startswith('#'): continue
      (name, _, url) = line.partition(':')
      gen_redirect(name.strip(), url.strip())
